/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.boot.context.event;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.ErrorHandler;

/**
 * {@link SpringApplicationRunListener} to publish {@link SpringApplicationEvent}s.
 * <p>
 * Uses an internal {@link ApplicationEventMulticaster} for the events that are fired
 * before the context is actually refreshed.
 *
 * @author Phillip Webb
 * @author Stephane Nicoll
 * @author Andy Wilkinson
 * @author Artsiom Yudovin
 * @since 1.0.0
 */
public class EventPublishingRunListener implements SpringApplicationRunListener, Ordered {

	/**
	 * 这个类很重要:
	 * 1.首先他会广播一个事件
	 * 		对应代码： for(final ApplicationListener : getApplicationListeners(event, type))
	 * 		getApplicationListeners 这两个参数就是事件类型、告诉所有的监听器现有一个 type 的 event、你们都感兴趣吗
	 *
	 * 2 告诉监听器：
	 * 	getApplicationListeners 告诉所有监听器、循环遍历所有的监听器
	 * 	监听器接收到事件、判断自己是否需要处理
	 *
	 * 3. 监听器怎么知道自己是否需要处理？
	 * 	第一步：
	 * 		supportEventType(event)、smartListener.supportEventType   会返回一个bool、如果感兴趣加到一个list里面
	 *	第二步：
	 *		在监听器回调的时候、还可以进行对事件类型的判断、如果都不感兴趣就不执行了
	 *
	 * 4. 获得所有对这个事件感兴趣的监听器、遍历其 onApplicationEvent 方法、这里传入一个 ApplicationStartingEvent 事件过去
	 *
	 * 		在spring中定义了11个监听器
	 *
	 * 5. initialMulticaster 可以看到是 SimpleApplicationEventMulticaster类型的对象
	 * 	一个广播事件、一个是执行 listener的 onApplicationEvent 方法
	 */
	private final SpringApplication application;

	private final String[] args;

	/**
	 * 这个对象是用来广播的 : onApplicationEvent
	 *
	 * 1. 被通知的对象都实现 ApplicationListener 这个接口 重写 onApplicationEvent 就能接受到广播事件
	 *  这些接口都是implants
	 * 	org.springframework.context.ApplicationListener=\
	 *		 org.springframework.boot.ClearCachesApplicationListener,\
	 *		 org.springframework.boot.builder.ParentContextCloserApplicationListener,\
	 *		 org.springframework.boot.context.FileEncodingApplicationListener,\
	 *		 org.springframework.boot.context.config.AnsiOutputApplicationListener,\
	 *		 org.springframework.boot.context.config.ConfigFileApplicationListener,\
	 *		 org.springframework.boot.context.config.DelegatingApplicationListener,\
	 *		 org.springframework.boot.context.logging.ClasspathLoggingApplicationListener,\
	 *		 org.springframework.boot.context.logging.LoggingApplicationListener,\
	 *		 org.springframework.boot.liquibase.LiquibaseServiceLocatorApplicationListener
	 *
	 * 2. ApplicationEvent 抽象定义了都有什么事件
	 *
	 *
	 */
	private final SimpleApplicationEventMulticaster initialMulticaster;

	public EventPublishingRunListener(SpringApplication application, String[] args) {
		this.application = application;
		this.args = args;
		/**
		 * 新建一个事件广播器SimpleApplicationEventMulticaster对象
		 */
		this.initialMulticaster = new SimpleApplicationEventMulticaster();
		/**
		 * 遍历在构造SpringApplication对象时从 spring.factories 配置文件中获取的事件监听器
		 *
		 * 将从spring.factories配置文件中获取的事件监听器们添加到事件广播器initialMulticaster对象的相关集合中
		 */
		for (ApplicationListener<?> listener : application.getListeners()) {
			this.initialMulticaster.addApplicationListener(listener);
		}
	}

	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * 广播 multicast
	 */
	@Override
	public void starting() {
		this.initialMulticaster.multicastEvent(new ApplicationStartingEvent(this.application, this.args));
	}

	/**
	 * 广播 multicast
	 */
	@Override
	public void environmentPrepared(ConfigurableEnvironment environment) {
		this.initialMulticaster
				.multicastEvent(new ApplicationEnvironmentPreparedEvent(this.application, this.args, environment));
	}

	@Override
	public void contextPrepared(ConfigurableApplicationContext context) {
		this.initialMulticaster
				.multicastEvent(new ApplicationContextInitializedEvent(this.application, this.args, context));
	}

	@Override
	public void contextLoaded(ConfigurableApplicationContext context) {
		for (ApplicationListener<?> listener : this.application.getListeners()) {
			if (listener instanceof ApplicationContextAware) {
				((ApplicationContextAware) listener).setApplicationContext(context);
			}
			context.addApplicationListener(listener);
		}
		this.initialMulticaster.multicastEvent(new ApplicationPreparedEvent(this.application, this.args, context));
	}

	@Override
	public void started(ConfigurableApplicationContext context) {
		context.publishEvent(new ApplicationStartedEvent(this.application, this.args, context));
	}

	/**
	 * 发布内容
	 * @param context the application context.
	 */
	@Override
	public void running(ConfigurableApplicationContext context) {
		context.publishEvent(new ApplicationReadyEvent(this.application, this.args, context));
	}

	@Override
	public void failed(ConfigurableApplicationContext context, Throwable exception) {
		ApplicationFailedEvent event = new ApplicationFailedEvent(this.application, this.args, context, exception);
		if (context != null && context.isActive()) {
			// Listeners have been registered to the application context so we should
			// use it at this point if we can
			context.publishEvent(event);
		}
		else {
			// An inactive context may not have a multicaster so we use our multicaster to
			// call all of the context's listeners instead
			if (context instanceof AbstractApplicationContext) {
				for (ApplicationListener<?> listener : ((AbstractApplicationContext) context)
						.getApplicationListeners()) {
					this.initialMulticaster.addApplicationListener(listener);
				}
			}
			this.initialMulticaster.setErrorHandler(new LoggingErrorHandler());
			this.initialMulticaster.multicastEvent(event);
		}
	}

	private static class LoggingErrorHandler implements ErrorHandler {

		private static Log logger = LogFactory.getLog(EventPublishingRunListener.class);

		@Override
		public void handleError(Throwable throwable) {
			logger.warn("Error calling ApplicationEventListener", throwable);
		}

	}

}

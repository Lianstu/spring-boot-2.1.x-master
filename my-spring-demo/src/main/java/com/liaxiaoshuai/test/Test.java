package com.liaxiaoshuai.test;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.ConfigurationWarningsApplicationContextInitializer;
import org.springframework.boot.context.config.ConfigFileApplicationListener;
import org.springframework.boot.context.config.DelegatingApplicationListener;
import org.springframework.boot.context.event.EventPublishingRunListener;

public class Test {

	EventPublishingRunListener eventPublishingRunListener;

	/**
	 * EventPublishLister#
	 * 这个类很重要:
	 * 1.首先他会广播一个事件
	 * 		对应代码： for(final ApplicationListener : getApplicationListeners(event, type))
	 * 		getApplicationListeners 这两个参数就是事件类型、告诉所有的监听器现有一个 type 的 event、你们都感兴趣吗
	 *
	 * 2 告诉监听器：
	 * 	getApplicationListeners 告诉所有监听器、循环遍历所有的监听器
	 * 	监听器接收到事件、判断自己是否需要处理
	 *
	 * 3. 监听器怎么知道自己是否需要处理？
	 * 	第一步：
	 * 		supportEventType(event)、smartListener.supportEventType   会返回一个bool、如果感兴趣加到一个list里面
	 *	第二步：
	 *		在监听器回调的时候、还可以进行对事件类型的判断、如果都不感兴趣就不执行了
	 *
	 * 4. 获得所有对这个事件感兴趣的监听器、遍历其 onApplicationEvent 方法、这里传入一个 ApplicationStartingEvent 事件过去
	 *
	 * 		在spring中定义了11个监听器
	 *
	 * 5. initialMulticaster 可以看到是 SimpleApplicationEventMulticaster类型的对象
	 * 	一个广播事件、一个是执行 listener的 onApplicationEvent 方法
	 *
	 *
	 *
	 * ===============
	 *
	 * 0. EventPublishingRunListener implents SpringApplicationRunListener  这个是来广播的事件的
	 *
	 *
	 *
	 * 1. 被通知的对象都实现 ApplicationListener 这个接口 重写 onApplicationEvent 就能接受到广播事件
	 *
	 * 	实现类要这么写 泛性： SmartApplicationListener extends ApplicationListener<ApplicationEvent>
	 *
	 *  这些接口都是
	 * 	org.springframework.context.ApplicationListener=\
	 *		 org.springframework.boot.ClearCachesApplicationListener,\
	 *		 org.springframework.boot.builder.ParentContextCloserApplicationListener,\
	 *		 org.springframework.boot.context.FileEncodingApplicationListener,\
	 *		 org.springframework.boot.context.config.AnsiOutputApplicationListener,\
	 *		 org.springframework.boot.context.config.ConfigFileApplicationListener,\
	 *		 org.springframework.boot.context.config.DelegatingApplicationListener,\
	 *		 org.springframework.boot.context.logging.ClasspathLoggingApplicationListener,\
	 *		 org.springframework.boot.context.logging.LoggingApplicationListener,\
	 *		 org.springframework.boot.liquibase.LiquibaseServiceLocatorApplicationListener
	 *
	 * 2. ApplicationEvent 抽象定义了都有什么事件  ApplicationEvent EventObject
	 * 		SpringApplicationEvent  这个右键看所有实现类  就能看到所有的spring 相关的实现类
	 *
	 *
	 *
	 *
	 *	源码部分看： spring-boot那个项目
	 *
	 * 	笔记：这个博客写的还可以啦 ==> https://www.cnblogs.com/ymbj/tag/SpringBoot源码专题/
	 *
	 * 	博客(写的比上面的那个好)：https://blog.csdn.net/woshilijiuyi/article/details/82388509
	 *
	 *
	 * 	拉取配置的内容： 知识点点 Import 实现 BeanFactoryAware 可以做很多事情
	 *
	 * 	@SpringBootApplication @EnableAutoConfiguration @Import(AutoConfigurationImportSelector.class)
	 */

	ConfigFileApplicationListener configFileApplicationListener;


	/**
	 * spring 的启动类 里面有个run方法要看的
	 */
	SpringApplication springApplication;


	DelegatingApplicationListener  delegatingApplicationListener;
	/**
	 *
	 * Delegate 授权、委派
	 */

	/**
	 * ApplicationContextInitializer 用来处理后置处理器的
	 */
	ConfigurationWarningsApplicationContextInitializer configurationWarningsApplicationContextInitializer;

}
